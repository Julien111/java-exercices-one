/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.exceptionexo;

/**
 *
 * @author Julien
 */
public class MontantTropEleveeException extends Exception {

    public MontantTropEleveeException(String message) {
        super(message);
    }
    
}
