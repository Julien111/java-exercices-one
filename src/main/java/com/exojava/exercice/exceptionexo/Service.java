/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.exceptionexo;

/**
 *
 * @author Julien
 */
public class Service {
    
    public void transfer(int idCompte, int idCompteDesti, double montant)throws MontantInvalideException{
        if(montant < 0){
            throw new MontantInvalideException("Le montant est invalide");
        }
    }
    
    public void retrait(int idCompte, double montant) throws MontantInvalideException, MontantTropEleveeException{
        if(montant < 0){
            throw new MontantInvalideException("Le montant est invalide");
        }
        if(montant > 1000){
            throw new MontantTropEleveeException("Le montant est trop élevé.");
        }
    }
   
}
