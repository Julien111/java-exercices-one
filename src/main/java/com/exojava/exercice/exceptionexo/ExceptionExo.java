/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.exojava.exercice.exceptionexo;

/**
 *
 * @author Julien
 */
public class ExceptionExo {

    public static void main(String[] args) throws MontantInvalideException, MontantTropEleveeException {
        try {
            Service serv = new Service();
            
            serv.retrait(2, 1200);
            
        } catch (MontantInvalideException | MontantTropEleveeException e) {
            String message = e.getMessage();
            System.out.println(message);
        }
    }
}
