/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.exceptionexo;

/**
 *
 * @author Julien
 */
public class MontantInvalideException extends Exception {

    public MontantInvalideException(String message) {
        super(message);
    }
    
}
