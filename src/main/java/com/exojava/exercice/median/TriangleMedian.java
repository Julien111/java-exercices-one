/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.median;

import java.text.DecimalFormat;

/**
 *
 * @author Julien
 */
public class TriangleMedian {    
    
    public static void main(String[] args) {
        
        double x1 = (10 + 12 + 4) / 3;
        DecimalFormat df = new DecimalFormat("#.####");
        double newx1 = Double.parseDouble(df.format(x1));
                
        System.out.println(newx1);
    }
            
}
