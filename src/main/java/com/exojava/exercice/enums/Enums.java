/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.enums;

/**
 *
 * @author Julien
 */
public class Enums {
    
    public final static String ETAT_AU_PANIER="ETAT_AU_PANIER";
    public final static String ETAT_PAYE="ETAT_PAYE";
    public final static String ETAT_EXPEDIE="ETAT_PAYE";
    
    public enum Etat{
        AU_PANIER, PAYE, EXPEDIE,
    }    
    
    private Etat etat;
    
    public static void main(String[] args) {
        
        Enums e = new Enums();
        
        System.out.println( "->" + e.etat.toString() );
        Etat etat = Enums.Etat.valueOf("EXPEDIE");
    
        String strEtat = Enums.Etat.PAYE.toString();
    
        Etat[] tousLesEtats = Enums.Etat.values();
        
        for (Etat element : tousLesEtats) {
            System.out.println(element);
        }
    }
    
}
