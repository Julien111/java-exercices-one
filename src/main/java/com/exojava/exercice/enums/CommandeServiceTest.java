/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.enums;

/**
 *
 * @author Julien
 */
public class CommandeServiceTest {
    public static void main(String[] args) {
        try {
            CommandeService serv = new CommandeService();
            Commande cmd = serv.creerCommande("Julien");
        
            serv.payerCommande(cmd);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }        
    }
}
