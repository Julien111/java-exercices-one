/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.enums;

import com.exojava.exercice.exceptionexo.MontantInvalideException;
import java.sql.Time;
import java.time.LocalDateTime;

/**
 *
 * @author Julien
 */
public class CommandeService {
    
    public void payerCommande(Commande commande) throws MontantInvalideException, EtatInvalideException{
        if(commande.getMontant() <= 0 ){
            throw new MontantInvalideException("Le montant de la commande doit être positif.");
        }
        if(commande.getEtat() != Commande.Etat.AU_PANIER){
            throw new EtatInvalideException("Impossible de payer un commande qui n'est pas au panier");
        }
        commande.setEtat(Commande.Etat.PAYEE);
    }
    
    public void expedierCommande(Commande commande){
        commande.setEtat(Commande.Etat.EXPEDIEE);
    }
    
    public Commande creerCommande(String nomClient){
        Commande nouvelleCommande = new Commande(Commande.Etat.AU_PANIER, nomClient,LocalDateTime.now(), 0.0);
        return nouvelleCommande;
    }
}