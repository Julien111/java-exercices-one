/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.enums;

import java.time.LocalDateTime;

/**
 *
 * @author Julien
 */
public class Commande {
    
    public enum Etat{
        AU_PANIER, PAYEE, EXPEDIEE
    }
    
    private Etat etat;
    
    private String nomClient;
    
    private LocalDateTime date;
    
    private Double montant;

    public Commande(Etat etat, String nomClient, LocalDateTime date, Double montant) {
        this.etat = etat;
        this.nomClient = nomClient;
        this.date = date;
        this.montant = montant;
    }
    
    
    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public String getNomClient() {
        return nomClient;
    }

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }
    
    
}
