/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.enums;

/**
 *
 * @author Julien
 */
public class EtatInvalideException extends Exception {

    public EtatInvalideException(String msg) {
        super(msg);
    }
    
}
