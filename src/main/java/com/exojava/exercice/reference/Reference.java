/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.reference;

/**
 *
 * @author Julien
 */
public class Reference {
    public static void main(String[] args) {
        Integer number = 5;     
        String words = "Salut";
        Voiture renault = new Voiture("red", "renault", "essence");
        
        System.out.println(renault.couleur);
        System.out.println(Reference.modifierVoiture(renault));
        System.out.println(renault.couleur);
         
        System.out.println(Reference.retourneInt(number));
        System.out.println(number);
    }
    
    public static double retourneDouble(double num){
        num= num * 5;
        return num;
    }
    
    public static Integer retourneInt(Integer num){
        num= (num * 2);
        return num;
    }
    
    public static String retourneStr(String words){
        words = "Hello";
        return words;
    }
    
    public static Voiture modifierVoiture(Voiture voiture){
        voiture.couleur = "blue";
        return voiture;
    }
}
