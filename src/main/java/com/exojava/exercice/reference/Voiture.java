/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.exojava.exercice.reference;

/**
 *
 * @author Julien
 */
public class Voiture {
    
    public String couleur;
    
    private String marque;
    
    private String typeMoteur;

    public Voiture(String couleur, String marque, String typeMoteur) {
        this.couleur = couleur;
        this.marque = marque;
        this.typeMoteur = typeMoteur;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getTypeMoteur() {
        return typeMoteur;
    }

    public void setTypeMoteur(String typeMoteur) {
        this.typeMoteur = typeMoteur;
    }    
}
